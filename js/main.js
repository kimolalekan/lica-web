/* Axuta Theme Scripts */

(function($) {
  "use strict";

  $(window).on("load", function() {
    $("body").addClass("loaded");
  });

  /*=========================================================================
	Sticky Header
=========================================================================*/

  $(function() {
    var header = $("#header"),
      yOffset = 0,
      triggerPoint = 80;
    $(window).on("scroll", function() {
      yOffset = $(window).scrollTop();

      if (yOffset >= triggerPoint) {
        header.addClass("navbar-fixed-top");
      } else {
        header.removeClass("navbar-fixed-top");
      }
    });
  });

  /*=========================================================================
        Mobile Menu
=========================================================================*/

  $(".menu-wrap ul.nav").slicknav({
    prependTo: ".header-section .navbar",
    label: "",
    allowParentLinks: true
  });

  /*=========================================================================
    Screenshot Carousel
=========================================================================*/
  function getSlide() {
    var wW = $(window).width();
    if (wW < 991) {
      return 1;
    } else if (wW < 1170) {
      return 3;
    } else {
      return 5;
    }
  }
  function getSlideSpace() {
    var wW = $(window).width();
    if (wW < 991) {
      return 0;
    }
    return 20;
  }
  var swiper = new Swiper(".swiper-container", {
    slidesPerView: getSlide(),
    loop: true,
    autoplay: true,
    centeredSlides: true,
    navigation: {
      nextEl: ".swiper-next",
      prevEl: ".swiper-prev"
    },
    spaceBetween: getSlideSpace()
  });

  /*=========================================================================
	Initialize smoothscroll plugin
=========================================================================*/
  smoothScroll.init({
    offset: 60
  });

  /*=========================================================================
	Scroll To Top
=========================================================================*/

  $(window).on("scroll", function() {
    if ($(this).scrollTop() > 100) {
      $("#scroll-to-top").fadeIn();
    } else {
      $("#scroll-to-top").fadeOut();
    }
  });

  /*=========================================================================
	WOW Active
=========================================================================*/

  new WOW().init();

  /*=========================================================================
	MAILCHIMP
=========================================================================*/

  if ($(".subscribe_form").length > 0) {
    /*  MAILCHIMP  */
    $(".subscribe_form").ajaxChimp({
      language: "es",
      callback: mailchimpCallback,
      url:
        "//alexatheme.us14.list-manage.com/subscribe/post?u=48e55a88ece7641124b31a029&amp;id=361ec5b369"
    });
  }

  function mailchimpCallback(resp) {
    if (resp.result === "success") {
      $("#subscribe-result").addClass("subs-result");
      $(".subscription-success")
        .text(resp.msg)
        .fadeIn();
      $(".subscription-error").fadeOut();
    } else if (resp.result === "error") {
      $("#subscribe-result").addClass("subs-result");
      $(".subscription-error")
        .text(resp.msg)
        .fadeIn();
    }
  }
  $.ajaxChimp.translations.es = {
    submit: "Submitting...",
    0: "We have sent you a confirmation email",
    1: "Please enter your email",
    2: "An email address must contain a single @",
    3: "The domain portion of the email address is invalid (the portion after the @: )",
    4: "The username portion of the email address is invalid (the portion before the @: )",
    5: "This email address looks fake or invalid. Please enter a real email address"
  };

  const faq = [
    {
      title: "What is Lica?",
      content:
        "Lica is an easy to use, emergency loan app, where you can apply for loan. The loan is approved &amp; disbursed within 5 minutes’ subject to you meeting our set criteria."
    },
    {
      title: "What information do I provide on Lica?",
      content:
        "Lica asks for basic information about you including your account number and your BVN"
    },
    {
      title: "What does Lica need my information for?",
      content:
        "Your information is required to determine your eligibility for the loan you are applying for"
    },
    {
      title:
        "What type of collateral or documents do I provide in support of my loan request?",
      content:
        "You do not need any collateral or documents to support your loan request. Once you provide your basic details (name, phone number, date of birth, account details and BVN), we will communicate to you in less than 5 minutes our loan decision and disbursement takes place immediately"
    },
    {
      title: "How do I know my BVN?",
      content:
        "You can obtain your BVN via the phone number currently linked to your bank account. Just dial *565*0#. "
    },
    {
      title: "How does Lica work?",
      content:
        "It’s simple. Go to Google Playstore and download the app. Provide your basic details and you are few minutes away from accessing the loan. Lica eliminates the difficulties associated with lending, i.e. documentations, collaterals, countless visits to loan providers etc. All you need to qualify for a loan is your phone number plus a few details, and the entire process is completed on your phone within 5 minutes."
    },
    {
      title: "I don’t have a smart phone; can I still apply for a Lica loan?",
      content:
        "Unfortunately, the only platform through which you can access Lica loan is through the Lica lending app available for download via the Google Play Store. However, we are working on making Lica loans available via other channels Once, these become available, our information page will be updated accordingly."
    },
    {
      title: "Will I be given a customer ID?",
      content:
        "Lica is an easy to use app. In keeping with this mantra, your mobile phone number which is unique to you will be the distinctive identity on the Lica app, as it will be your user ID. This unique identifier will be used in all correspondence with you. Also, once you download the app, your activation code is sent to you via SMS on this number. For issue resolutions, and any other support service request, your mobile phone number will be required for easy identification."
    },
    {
      title: "How do I check my account balance(s) or loan details?",
      content:
        "Simply log in to your Lica app to view all details regarding your loan."
    },
    {
      title: "How do I apply for a loan on Lica?",
      content:
        "Go to Google Playstore and download the Lica app. After downloading the app, sign up and provide your basic details. This typically takes few minutes to complete. A verification code will be sent to your phone via SMS, which you will use to complete your registration. Once complete, you can then proceed to request for a loan. A maximum and minimum amount that you can borrow will be displayed and you can then decide how much you would like to borrow. Once you state the amount, your expected one-off repayment will be displayed. You then proceed to accept the terms and conditions of the loan. The decision on your loan request will be communicated to you via SMS in few minutes"
    },
    {
      title: "Is there any restriction to Lica app anywhere in Nigeria? ",
      content:
        "NO! Lica is currently available everywhere in Nigeria. All you need is a smartphone and an internet access. With these two, you are good to go."
    },
    {
      title:
        "Lica is requesting for my Bank Verification Number (BVN), what for?",
      content:
        "Requesting for your BVN is a form of security check for us. We want to be sure that the individual applying for loan on Lica is the owner of the account number provided. The BVN provided helps us validate that the owner of the account number provided is the same person using the Lica service. "
    },
    {
      title:
        "Is Lica permitted to carry out this service? How do you reach us?",
      content:
        "Lica is a lending service offered by Modellica Partners Ltd (RC: 1564485), a company licensed by the Lagos State Government to offer lending services. Our services are currently available solely online. We can be reached via our various customer care platforms (telephone, emails, SMS and social media handles)"
    },
    {
      title: "How much can I borrow from Lica and for how long?",
      content:
        "The maximum and minimum amount you can borrow is determined by our system, which uses the information that you provided. The loan is to be repaid at your next salary payment. Our loans are provided to meet emergency basic needs, hence the short repayment period. To increase your chance of qualifying and accessing Lica loan, ensure that the information is accurate and you are applying from your personal mobile device."
    },
    {
      title: "I was approved for a loan, why have I not yet received my funds?",
      content:
        "The delay might be due to system downtime from our payment processors. However, if you do not receive a credit alert within 6 hours, please contact our customer service team."
    },
    {
      title:
        "Why did I got a message; “Your loan cannot be approved at this time. Please try again later?",
      content:
        "You got the message because your salary is not currently processed by Remita. Lica loan is available to customers whose salaries are processed by Remita for now."
    },
    {
      title: "How do I know if my salary is processed by Remita?",
      content:
        "Kindly find out from your employer to know if Remita is responsible for processing your salary."
    },
    {
      title: "",
      content: ""
    },
    {
      title: "Why was my loan application on Lica rejected?",
      content:
        "Your loan was likely rejected because we do not have sufficient and correct information about you to make a loan decision. It is important that you provide all correct and required information about yourself."
    },
    {
      title: "Can I re-apply for Lica loan having been rejected before?",
      content:
        "Yes, you can re-apply. Just ensure that you have and keep accurate information about yourself."
    },
    {
      title: "How do I know the status of my loan request? ",
      content:
        "Once you submit your application for loan, a notification will be sent to you via SMS stating our decision on your application. If you do not receive SMS within 5 minutes of submitting your application, kindly check the ‘View Existing Borrowings’ menu in the app to confirm the current status of your request."
    },
    {
      title: "How long does the application process for my Lica loan take?",
      content:
        "We typically give a decision on your loan within 5 minutes, while disbursement is made immediately to your account. However, in extreme cases, we will complete within 24 hours."
    },
    {
      title:
        "Can I apply for a higher amount more than the current loan offer?",
      content:
        "The current loan offer is what you have been assessed to qualify for at this time. However, you may be able to take a higher amount in the future based on your good credit score and prompt repayment of loans."
    },
    {
      title:
        "My loan status is showing as ‘Pending’ after I have submitted my application. How do I proceed?",
      content:
        "This status indicates that our system is processing your application. You will receive a notification on your loan decision shortly after."
    },
    {
      title: "I have a Lica loan, how do I repay?",
      content:
        "Your loan repayment will be automatically deducted from your salary account when your next salary is paid by your employer."
    },
    {
      title:
        "How soon can I apply for a new Lica loan having paid back my previous loan?",
      content:
        "You can apply for a new loan as soon as you repay your previous loan. However, check “View Existing Borrowings” on the Lica loan app to be sure you have successfully paid back your previous loan."
    },
    {
      title: "Can I repay my loan after due date for repayment?",
      content:
        "Kindly note that we will try to collect your repayment when your salary following when you borrow money is paid. However, if we are unable to collect or your salary cannot cover your repayment, we will collect at the next salary and interest will be charged on the uncollected portion."
    },
    {
      title: "Can Lica be trusted with my personal details?",
      content:
        "Your personal data is secure when using the Lica app as we use world-class data security and encryption techniques to protect the data you share with us. Also, your personal details with Lica are never shared with third parties. Your transaction with Lica is strictly confidential. For more details, please check our privacy policy. \n When using Lica, your data is always secure. Our platform uses world-class standard security and encryption technology to protect all details. When you provide us with information, you can also be assured of your privacy – our loan agreements are always kept confidential."
    },
    {
      title: "What are Lica Terms of Use and Privacy Policy?",
      content:
        "Please read our Terms of Use and Privacy Policy via the links below. <br/><a href='/privacy'>Privacy Policy</a><br/><a href='/'>Terms & Conditions</a>"
    }
  ];
  let faqData = "";
  faq.forEach(function(item) {
    faqData +=
      '<div class="accordion"><span style="float: right;"><i class="fa fa-chevron-down"></i></span>' +
      item.title +
      '<p class="content">' +
      item.content +
      "</p></div>";
  });

  $(".faq").html(faqData);

  var allPanels = $(".faq > .accordion > .content").hide();

  $(".faq > .accordion").click(function() {
    allPanels.slideUp();
    $(this)
      .children()
      .slideDown();
    return false;
  });
})(jQuery);
